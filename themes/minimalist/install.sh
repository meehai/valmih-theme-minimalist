function args {
	if [ $# -ne 1 ]; then
		echo "Usage: ./install <valmih_CMS_path>";
		exit 1;
	fi

	SCRIPT=`realpath $0`
	export PWD=`dirname $SCRIPT`
	export vmPath=$1;

	if [ ${vmPath: -1} == "/" ]; then
		export vmPath=${vmPath:0:-1};
	fi
}

function installReplace {
	echo "[$moduleName] Installing $1 to $2"
	target=$2
	unlink $2;
	ln -s $1 $2
}

export moduleName="minimalist"
args "$@"
echo "[minimalist] Installing to $vmPath";

installReplace $PWD/themes/minimalist $vmPath/themes/minimalist

echo "[minimalist] Done!"

